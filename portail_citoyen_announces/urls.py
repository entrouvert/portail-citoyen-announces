from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'portail_citoyen_announces.views.homepage_view',
        name='announce-homepage'),
    url(r'^subscribe/$', 'portail_citoyen_announces.views.subscription_view',
        name='announce-subscribe'),
    url(r'^feed/$', 'portail_citoyen_announces.views.feed',
        name='announce-feed'),
)
