from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from django.contrib.syndication.views import Feed
from django.contrib.sites.models import get_current_site


from django.utils.feedgenerator import Atom1Feed


import forms
import models
import transports
import app_settings


class SubscriptionView(FormView):
    template_name = 'portail_citoyen_announces/subscription_edit.html'
    form_class = forms.SubscriptionForm
    success_url = '..'

    def get_form_kwargs(self, **kwargs):
        kwargs = super(SubscriptionView, self).get_form_kwargs(**kwargs)
        kwargs['user'] = self.request.user
        kwargs['site'] = get_current_site(self.request)
        return kwargs

    def form_valid(self, form):
        form.save()
        return super(SubscriptionView, self).form_valid(form)


subscription_view = SubscriptionView.as_view()

class AnnounceHomepageView(ListView):
    model = models.Announce
    template_name = 'portail_citoyen_announces/announce_homepage.html'

    def get_queryset(self):
        qs = models.Announce.objects.all().published()
        if app_settings.feed_homepage_limit:
            qs = qs[:app_settings.feed_homepage_limit]
        return qs

    def get_context_data(self, **kwargs):
        ctx = super(AnnounceHomepageView, self).get_context_data(**kwargs)
        ctx['id_prefix'] = 'announce-item-'
        if self.request.user.is_authenticated():
            subscriptions = models.Subscription.objects.filter(
                user=self.request.user).select_related('category')
            ctx['subscriptions'] = [ sub.category.name for sub in subscriptions ]
        return ctx

homepage_view = AnnounceHomepageView.as_view()


class AnnounceFeed(Feed):
    title = app_settings.feed_title
    description = app_settings.feed_description
    link = app_settings.feed_link
    feed_item_link_template = app_settings.feed_item_link_template
    feed_type = Atom1Feed

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def items(self):
        return models.Announce.objects.published().order_by('-publication_time')

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.text

    def item_link(self, item):
        return self.feed_item_link_template.format(item.pk)

    def item_pubdate(self, item):
        return item.publication_time or item.modification_time


feed = AnnounceFeed()
