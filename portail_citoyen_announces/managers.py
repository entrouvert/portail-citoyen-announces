from model_utils.managers import PassThroughManager
from django.db.models.query import QuerySet
from django.db.models import Q
try:
    from django.utils.timezone import now as now
except ImportError:
    import datetime
    now = datetime.now

class AnnounceQueryset(QuerySet):
    def published(self):
        qs = self.filter(hidden=False)
        qs = qs.filter(
                (Q(publication_time__lte=now) | Q(publication_time__isnull=True)) &
                (Q(expiration_time__gte=now) | Q(expiration_time__isnull=True)))
        return qs

    def unpublished(self):
        qs = self.exclude(hidden=False)
        qs = qs.exclude(
                (Q(publication_time__lte=now) | Q(publication_time__isnull=True)) &
                (Q(expiration_time__gte=now) | Q(expiration_time__isnull=True)))
        return qs


AnnounceManager = PassThroughManager.for_queryset_class(AnnounceQueryset)
