from django.conf import settings

default_transport_modes = (
        'portail_citoyen_announces.transports.EmailTransport',
        'portail_citoyen_announces.transports.HomepageTransport',
)

transport_modes = getattr(settings, 'ANNOUNCES_TRANSPORTS',
        default_transport_modes)

default_from = getattr(settings, 'ANNOUNCES_DEFAULT_FROM_EMAIL', None)

feed_title = getattr(settings, 'ANNOUNCES_FEED_TITLE', u'Announces')
feed_description = getattr(settings, 'ANNOUNCES_FEED_TITLE',
    u'')
feed_link = getattr(settings, 'ANNOUNCES_FEED_LINK', '')
feed_item_link_template = getattr(settings,
    'ANNOUNCES_FEED_ITEM_LINK_TEMPLATE', '#announce-item-{0}')
feed_homepage_limit = getattr(settings, 'ANNOUNCES_FEED_HOMEPAGE_LIMIT', 10)
