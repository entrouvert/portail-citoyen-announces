from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.validators import RegexValidator
from django.utils import timezone


from model_utils.models import TimeStampedModel


import managers
import transports


class Category(TimeStampedModel):
    site = models.ForeignKey('sites.Site', verbose_name=_('site'))
    name = models.CharField(_('name'), max_length=64)
    identifier = models.CharField(_('identifier'),
            unique = True,
            max_length=64,
            validators=[RegexValidator(r'^[a-z0-9_]+$')],
            help_text=_('the identifier is used to find '
                'templates for sending announces; it can '
                'only contain lowercase letter, digits or underscores'))

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('category')
        ordering = ('name',)
        unique_together = (('site', 'name'), ('site', 'identifier'))


class Announce(models.Model):
    objects = managers.AnnounceManager()

    title = models.CharField(_('title'), max_length=256,
        help_text=_('maximum 256 characters'))
    text = models.TextField(_('text'))

    hidden = models.BooleanField(_('hidden'), blank=True, default=False)

    publication_time = models.DateTimeField(_('publication time'), blank=True,
            null=True, default=timezone.now)
    expiration_time = models.DateTimeField(_('expiration time'), blank=True,
            null=True)
    creation_time = models.DateTimeField(_('creation time'), auto_now_add=True)
    modification_time = models.DateTimeField(_('modification time'), auto_now=True)

    category  = models.ForeignKey('Category', verbose_name=_('category'))

    def __unicode__(self):
        return u'{title} ({id}) at {modification_time}'.format(title=self.title,
                id=self.id, modification_time=self.modification_time)

    class Meta:
        verbose_name = _('announce')
        ordering = ('-modification_time',)


class Sent(models.Model):
    announce = models.ForeignKey('Announce', verbose_name=_('announce'))
    # subscription = models.ForeignKey('Subscription', verbose_name=_('subscription'))
    transport = models.CharField(_('transport'), max_length=32,
            choices=transports.get_transport_choices(), blank=False)
    time = models.DateTimeField(_('sent time'), auto_now_add=True)
    result = models.TextField(_('result'), blank=True)

    def __unicode__(self):
        return u'announce {id} sent via {transport} at {time}'.format(
                id=self.announce.id, transport=self.transport, time=self.time)

    class Meta:
        verbose_name = _('sent')
        ordering = ('-time',)


class Subscription(TimeStampedModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('user'),
            blank=True, null=True)
    category = models.ForeignKey('Category', verbose_name=_('category'))
    identifier = models.CharField(_('identifier'), max_length=128, blank=True,
            help_text=_('ex.: email, mobile phone number, jabber id'))
    transport = models.CharField(_('transport'),
            max_length=32, choices=transports.get_transport_choices(),
            blank=False)

    def __unicode__(self):
        return u'subscription of {user} to category {category} with ' \
                'transport {transport} and identifier {identifier}'.format(
                        user=self.user,
                        category=self.category,
                        transport=self.transport,
                        identifier=self.identifier)

    def real_identifier(self):
        transport = transports.get_transport(self.transport)
        return transport.get_identifier_from_subscription(self)
    real_identifier.short_description = _('identifier')

    def user_display_name(self):
        if not self.user:
            return _('anonymous')
        return self.user.get_full_name() or self.user.username
    user_display_name.short_description = _('user')

    class Meta:
        verbose_name = _('subscription')
        ordering = ('-created',)

try:
    from cms.models import CMSPlugin
except:
    pass
else:
    class AnnounceListPlugin(CMSPlugin):
        pass

    class AnnounceSubscribePlugin(CMSPlugin):
        pass
