from cms.plugin_base import CMSPluginBase


from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.sites.models import get_current_site


from cms.plugin_pool import plugin_pool


import forms
import models
import transports
import app_settings


class AnnounceListPlugin(CMSPluginBase):
    model = models.AnnounceListPlugin
    name = _('Announce List Plugin')
    render_template = "portail_citoyen_announces/announce_list_plugin.html"
    text_enabled = True
    transport_identifier = transports.HomepageTransport.identifier

    def get_queryset(self, user):
        qs = models.Announce.objects.published()
        categories = models.Subscription.objects.filter(
                transport=self.transport_identifier,
                user=user).values_list('category', flat=True)
        qs = qs.filter(category__in=categories)
        if app_settings.feed_homepage_limit:
            qs = qs[:app_settings.feed_homepage_limit]
        return qs

    def render(self, context, instance, placeholder):
        request = context['request']
        context['object_list'] = self.get_queryset(request.user)
        context['id_prefix'] = 'announce-item-'
        subscriptions = models.Subscription.objects.filter(
                transport=self.transport_identifier,
                user=request.user).select_related('category')
        context['subscriptions'] = [ sub.category.name for sub in subscriptions ]
        return context

    def icon_src(self, instance):
        return settings.STATIC_URL + u"compte_meyzieu/meyzieu_newsletters_plugin.png"

class FormPluginMixin(object):
    form_class = None
    success_msg = None
    render_template = 'portail_citoyen_announces/form_plugin.html'

    def get_form_kwargs(self, context, instance, placeholder):
        request = context['request']
        return dict(user=request.user, site=get_current_site(request))

    def render(self, context, instance, placeholder):
        request = context['request']
        context['submit'] = submit = 'cms-form-plugin-%s' % instance.id
        if request.method == 'POST' and submit in request.POST:
            form  = self.form_class(data=request.POST,
                    **self.get_form_kwargs(context, instance, placeholder))
            if form.is_valid():
                form.save()
                context['success'] = self.success_msg
        else:
            form  = self.form_class(**self.get_form_kwargs(context, instance,
                placeholder))
        context['form'] = form
        return context


class AnnounceSubscribePlugin(FormPluginMixin, CMSPluginBase):
    model = models.AnnounceSubscribePlugin
    form_class = forms.SubscriptionForm
    name = _('Announce Subscribe Plugin')
    sucess_msg = _(u'Your subscriptions were saved')
    text_enabled = True


plugin_pool.register_plugin(AnnounceSubscribePlugin)
plugin_pool.register_plugin(AnnounceListPlugin)
