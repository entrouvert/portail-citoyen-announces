import os

from django.contrib import admin
from django.contrib.sites.models import get_current_site
from django.db import connection

import models
import transports
from django.utils.translation import ugettext_lazy as _


class SendingAction(object):
    __name__ = 'sending action'

    def __init__(self, mode):
        self.mode = mode
        self.__name__ = mode

    @property
    def short_description(self):
        transport = transports.get_transport(self.mode)
        display_name = getattr(transport, 'display_name', transport.identifier)
        return _('Send by {0}').format(display_name)

    def __call__(self, modeladmin, request, queryset):
        pid = os.fork()
        if pid != 0:
            connection.close()
            return
        connection.close()
        transport = transports.get_transport(self.mode)
        for announce in queryset.select_related():
            transport.send(announce)


def transport_actions():
    return [SendingAction(transport.identifier)
            for transport in transports.get_transports() if hasattr(transport, 'send')]


class SentInlineAdmin(admin.TabularInline):
    model = models.Sent
    extra = 0
    can_delete = False
    fields = [ 'time', 'transport', 'result' ]
    readonly_fields = [ 'time', 'transport', 'result' ]

    def has_add_permission(self, request):
        return False


class AnnounceAdmin(admin.ModelAdmin):
    actions = transport_actions()
    list_display = ['title', 'category', 'extract', 'publication_time',
            'expiration_time', 'hidden', 'sent' ]
    inlines = ( SentInlineAdmin, )

    def extract(self, instance):
        return instance.text[:60]
    extract.short_description = _('extract')

    def sent(self, instance):
        return u', '.join(instance.sent_set.values_list('transport', flat=True).distinct())
    sent.short_description = _('sent')


class CategoryAdmin(admin.ModelAdmin):
    list_display = [ 'name', 'identifier' ]

    def get_queryset(self, request):
        qs = super(CategoryAdmin, self).get_queryset(request)
        qs.filter(site=get_current_site(request))
        return qs

class SubscriptionAdmin(admin.ModelAdmin):
    list_filter = [ 'category' ]
    list_display = [ 'category', 'transport', 'real_identifier', 'user_display_name', 'created' ]


admin.site.register(models.Announce, AnnounceAdmin)
admin.site.register(models.Category, CategoryAdmin)
admin.site.register(models.Subscription, SubscriptionAdmin)
